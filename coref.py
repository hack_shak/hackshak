import nltk
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer
from nltk.corpus import wordnet

train_text = state_union.raw("2005-GWBush.txt")
sample_text = state_union.raw("k1.txt")

custom_sent_tokenizer = PunktSentenceTokenizer(train_text)

tokenized = custom_sent_tokenizer.tokenize(sample_text)

def process_content():
    try:
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            namedEnt= nltk.ne_chunk(tagged)             
            chunkGram = r"""Chunk: {<NN.*>{1,5}}"""
            chunkParser = nltk.RegexpParser(chunkGram)
            chunked = chunkParser.parse(namedEnt)
                     
            for s in chunked.flatten() :                
                if s[1] == 'NNP':
                    print (s[0])
                  
                   
                if s[0] =='.':
                    print("  ")
                
             

            

    except Exception as e:
        print(str(e))

process_content()
